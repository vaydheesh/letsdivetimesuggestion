import copy
import json

from django.test import TestCase
from rest_framework import status

from users.models import User

from .constant import Constant


class SuggestedTimeTestCase(TestCase):
    """
    Test the song endpoints.
    """

    def setUp(self):
        self.user1 = User.objects.create(
            first_name="John",
            last_name="Doe",
            company="CompanyABC",
            designation="Employee",
            start_time="07:30:00",
            end_time="16:00:00",
            timezone="Asia/Kolkata",
        )

        self.user2 = User.objects.create(
            first_name="Jane",
            last_name="Doe",
            company="CompanyXYZ",
            designation="Employee",
            start_time="6:00:00",
            end_time="18:00:00",
            timezone="Asia/Kolkata",
        )

        self.user3 = User.objects.create(
            first_name="Alpha",
            last_name="User",
            company="CompanyXYZ",
            designation="Employee",
            start_time="00:00:00",
            end_time="05:00:00",
            timezone="Asia/Kolkata",
        )

        self.valid_payload = {
            "1": {
                "calendars": {
                    "primary": {
                        "busy": [
                            {
                                "start": "2022-06-04T08:00:00+05:30",
                                "end": "2022-06-04T09:00:00+05:30",
                            },
                        ]
                    }
                }
            },
            "2": {
                "calendars": {
                    "primary": {
                        "busy": [
                            {
                                "start": "2022-06-04T10:00:00+05:30",
                                "end": "2022-06-04T11:00:00+05:30",
                            },
                        ]
                    }
                }
            },
            "3": {
                "calendars": {
                    "primary": {
                        "busy": [
                            {
                                "start": "2022-06-04T11:00:00+05:30",
                                "end": "2022-06-04T11:45:00+05:30",
                            },
                        ]
                    }
                }
            },
        }

    def tearDown(self):
        self.user1.delete()
        self.user2.delete()
        self.user3.delete()

    def test_suggested_time_valid(self):
        url = "http://0.0.0.0:8000/api/v1/suggested_time/?users=1,2&duration_mins=30&count=3"
        response = self.client.post(
            url, data=json.dumps(self.valid_payload), content_type="application/json"
        )

        valid_response = [
            {"start": "2022-06-04T02:00:00Z", "end": "2022-06-04T02:30:00Z"},
            {"start": "2022-06-04T03:30:00Z", "end": "2022-06-04T04:00:00Z"},
            {"start": "2022-06-04T04:00:00Z", "end": "2022-06-04T04:30:00Z"},
        ]

        self.assertJSONEqual(str(response.content, encoding="utf8"), valid_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_suggested_time_no_common_time(self):
        url = "http://0.0.0.0:8000/api/v1/suggested_time/?users=1,2,3&duration_mins=30&count=3"
        response = self.client.post(
            url, data=json.dumps(self.valid_payload), content_type="application/json"
        )

        expected_response = {Constant.msg: Constant.no_common_time_msg}

        self.assertJSONEqual(str(response.content, encoding="utf8"), expected_response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_suggested_time_no_busy_time(self):
        payload = copy.deepcopy(self.valid_payload)
        payload["1"]["calendars"]["primary"]["busy"] = []
        payload["2"]["calendars"]["primary"]["busy"] = []
        url = "http://0.0.0.0:8000/api/v1/suggested_time/?users=1,2&duration_mins=30&count=3"
        response = self.client.post(
            url, data=json.dumps(payload), content_type="application/json"
        )

        valid_response = [
            {"start": "2020-11-12T02:00:00Z", "end": "2020-11-12T02:30:00Z"},
            {"start": "2020-11-12T02:30:00Z", "end": "2020-11-12T03:00:00Z"},
            {"start": "2020-11-12T03:00:00Z", "end": "2020-11-12T03:30:00Z"},
        ]

        self.assertJSONEqual(str(response.content, encoding="utf8"), valid_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_suggested_time_all_day_busy(self):
        payload = copy.deepcopy(self.valid_payload)
        payload["1"]["calendars"]["primary"]["busy"] = [
            {
                "start": "2022-06-04T07:30:00+05:30",
                "end": "2022-06-04T16:00:00+05:30",
            },
        ]
        payload["2"]["calendars"]["primary"]["busy"] = [
            {
                "start": "2022-06-04T06:00:00+05:30",
                "end": "2022-06-04T18:00:00+05:30",
            },
        ]
        url = "http://0.0.0.0:8000/api/v1/suggested_time/?users=1,2&duration_mins=30&count=3"
        response = self.client.post(
            url, data=json.dumps(payload), content_type="application/json"
        )

        valid_response = []
        self.assertJSONEqual(str(response.content, encoding="utf8"), valid_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_suggested_time_invalid_params(self):
        url = "http://0.0.0.0:8000/api/v1/suggested_time/?users=1a,2&duration_mins=30&count=3"
        response = self.client.post(
            url, data=json.dumps(self.valid_payload), content_type="application/json"
        )
        expected_response = {Constant.msg: Constant.invalid_query_params}
        self.assertJSONEqual(str(response.content, encoding="utf8"), expected_response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
