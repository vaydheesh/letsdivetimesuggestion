from django.urls import path

from .views import suggested_time

urlpatterns = [
    path("", suggested_time),
]
