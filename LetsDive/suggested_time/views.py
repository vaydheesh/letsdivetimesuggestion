import json

from django.http import QueryDict
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from .common_time.utils import calculate_fun_time_interval
from .common_time.Errors import NoCommonTimeError
from .constant import Constant


@api_view(["POST"])
def suggested_time(request: Request):
    """
    Suggest at most `count` number,
    of free time slots from list of users provided in query params,
    for duration of `duration_mins` in query params

    Free time lies between work timing of all users
    """
    params: QueryDict = request.query_params
    try:
        users = [int(i) for i in params["users"].split(",")]
        duration_mins = int(params["duration_mins"])
        count = int(params["count"])

        if not users or duration_mins <= 0 or count <= 0:
            raise ValueError

    except (KeyError, ValueError):
        return Response(
            {Constant.msg: Constant.invalid_query_params},
            status=status.HTTP_400_BAD_REQUEST,
        )
    calendar_timing = json.loads(request.body)

    try:
        ans = calculate_fun_time_interval(users, duration_mins, count, calendar_timing)
        return Response(ans)

    except NoCommonTimeError:
        return Response(
            {Constant.msg: Constant.no_common_time_msg},
            status=status.HTTP_400_BAD_REQUEST,
        )
