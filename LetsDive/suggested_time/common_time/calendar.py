from typing import Any

from .convert_utc import utc_busy_timing
from .time_models import BusyTime


def calendar_query(user: int, calendar_timing: dict[Any, Any]) -> list[BusyTime]:
    busy_schedules = calendar_timing[str(user)]["calendars"]["primary"]["busy"]
    return [utc_busy_timing(busy_schedule) for busy_schedule in busy_schedules]
