from datetime import datetime

import pytz

from .time_models import WorkTiming, BusyTime


def time_to_utc(local_time: str, timezone: str) -> datetime:
    # A random date to prevent default fallback to 1900/01/01
    # default date doesn't well work with current UTC time conversion
    local_time = f"12/11/2020 {local_time}"
    local = pytz.timezone(timezone)
    naive = datetime.strptime(local_time, "%d/%m/%Y %H:%M:%S")
    local_dt = local.localize(naive, is_dst=None)
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt


def utc_work_timing(user_time) -> WorkTiming:
    start_time = user_time["start_time"]
    end_time = user_time["end_time"]
    timezone = user_time["timezone"]
    uid = user_time["id"]
    utc_start_time = time_to_utc(start_time, timezone)
    utc_end_time = time_to_utc(end_time, timezone)
    return WorkTiming(utc_start_time, utc_end_time, uid)


def date_time_to_utc(local_time: str):
    local_date_time = datetime.fromisoformat(local_time)
    utc_date_time = local_date_time.astimezone(pytz.utc)
    return utc_date_time


def utc_busy_timing(user_time: dict[str, str]) -> BusyTime:
    start_time = user_time["start"]
    end_time = user_time["end"]
    utc_start_time = date_time_to_utc(start_time)
    utc_end_time = date_time_to_utc(end_time)
    return BusyTime(utc_start_time, utc_end_time)
