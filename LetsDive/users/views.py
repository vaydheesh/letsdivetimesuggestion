from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from .models import User
from .serializers import (
    UserDetailSerializer,
    UserTimingSerializer,
)


class UserDetailList(ListCreateAPIView):
    """List view of Users"""
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer


class UserDetail(RetrieveUpdateDestroyAPIView):
    """CRUD view of User Details"""
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer


class UserTimingDetail(RetrieveUpdateDestroyAPIView):
    """CRUD view of User Work Timings"""
    queryset = User.objects.all()
    serializer_class = UserTimingSerializer
