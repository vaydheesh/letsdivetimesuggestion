from django.db import models


class User(models.Model):
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, blank=False)
    company = models.CharField(max_length=50, blank=False)
    designation = models.CharField(max_length=50, blank=False)
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)
    timezone = models.CharField(max_length=50, default="Asia/Kolkata")
