from rest_framework import serializers

from .models import User


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "company",
            "designation",
        )


class UserTimingSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "start_time",
            "end_time",
            "timezone",
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"
